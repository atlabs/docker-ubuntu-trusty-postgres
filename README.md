# docker-ubuntu-postgres

Postgres on Ubuntu 14.04 with contrib and postgis packages installed.

# Building and Pushing

Create this image as follows:

        sudo docker build -t atlp/ubuntu-trusty-postgres:9.4 9.4/.

Now that our image is built, push it to the:

        sudo docker push atlp/ubuntu-trusty-postgres:9.4
